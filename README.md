# README #

This README is just here as a general overview for anyone that stumbles upon it.  It's not necessary at the moment as there is only one developer working on the site at the present time.

### What is this repository for? ###

* Code storage for Full Throttle Web Designs
* Site version 1.5

### Who do I talk to if I have questions? ###

* If you have any suggestions or comments you can contact Russell Nield at russell@ft-wd.com